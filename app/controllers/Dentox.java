package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import play.data.validation.*;

public class Dentox extends Controller {

    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            User user = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", user.fullname);
        }
    }
    public static void index() {
        System.out.println("DentoX2 is running !");
        render();
    }
    
    public static void Features() {
        System.out.println("DentoX2 :Features !");
        render();
    }
    
    public static void Gallery() {
        System.out.println("DentoX2 :Gallery !");
        render();
    }
    
    public static void Pricing() {
        System.out.println("DentoX2 :Pricing !");
        render();
    }    
    
    public static void ContactUs() {
        System.out.println("DentoX2 :Contact Us !");
        render();
    }

    public static void Post() {
        System.out.println("DentoX2 :Help Desk !");
        Post frontPost = Post.find("order by postedAt desc").first();
        List<Post> olderPosts = Post.find(
            "order by postedAt desc"
        ).from(1).fetch(10);
        render(frontPost, olderPosts);
    }
    
    public static void show(Long id) {
        Post post = Post.findById(id);
        render(post);
    }

    public static void postComment(Long postId, @Required String author, @Required String content) {
        Post post = Post.findById(postId);
        if (validation.hasErrors()) {
            render("Dentox/show.html", post);
        }
        post.addComment(author, content);
        show(postId);
    }



}