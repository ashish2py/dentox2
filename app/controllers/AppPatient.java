package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import play.data.validation.*;
import play.data.validation.Error.*;
import models.*;
import play.modules.search.*;
import static play.modules.pdf.PDF.*;


@With(Secure.class)
public class AppPatient extends Controller {

    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            User user = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", user.fullname);
        }
    }

    //Search patient
    public static void search(String search) {
    	Query query;
    	String highlight;
    	
    	if (search.contains(":")) {
    		query = Search.search(search, Patient.class);
    		highlight = search.split(":", 2)[1];
    	} else {
    		query = Search.search("firstName:"+search, Patient.class);
    		highlight = search;
    	}
    	List results = query.fetch();
    	renderTemplate("Patient/search.html",results, search, highlight);
    }
    

//index
    public static void index() {
        System.out.println("DentoX2 is running !!");
        renderTemplate("Patient/patient.html");
    }
//Table    
    public static void patient_table(){    
        System.out.println("DentoX2 : @ Patient Table !!");
        //List patients=Patient.findAll();
        List<Patient> patients=Patient.find(
            "order by id asc"
        ).from(0).fetch(20);
        render("Patient/patient_table.html",patients);
    }
//List        
    public static void patientList(){
        System.out.println("DentoX2 : @ Patient List !!");
        render("Patient/patientList.html");
    }
//Charting
    public static void patientCharting(){
            System.out.println("DentoX2 : @ Patient Charting !!");
            render("Patient/patient_charting.html");
    }
//Documents
    public static void patientDocument(){
	List<Document> documents=Document.find(
	        "order by id asc"
	    ).from(0).fetch(10);
            System.out.println("DentoX2 : @ Patient Documents !!");
            render("Patient/patient_documents.html");
    }
//Reminders    
    public static void patientReminder(){
            System.out.println("DentoX2 : @ Patient Reminder !!");
            List<PatientTask> patientTask=PatientTask.find(
                "order by id asc"
            ).from(0).fetch(20);
            render("Patient/patient_reminder.html",patientTask);
    }
    
    
//Search Patient
/*
    public static void search(String firstName){
        List<Patient> patients = Patient.find("byfirstName",firstName).fetch(100);
        renderTemplate("AppPatient/searchlist.html",patients);
    }
*/

//Form
    public static void form(Long id) {
        if(id != null) {
            Patient patient = Patient.findById(id);
            render("Patient/Admin/form.html",patient);
        }
        renderTemplate("Patient/Admin/form.html");
    }

//Save patient
    public static void save(Long id,
                    String firstName,String lastName,String address,
                    Long phoneNumber,Date birthAt,String gender,
                    String healthStatus,String hin,
                    Date visitedAt
                    ) {
        Patient patient;
        if(id == null) {
            // Create patient
            User author = User.find("byEmail", Security.connected()).first();
            patient = new Patient(author, firstName,
                                  lastName,address,
                                  phoneNumber,birthAt,
                                  gender,healthStatus,
                                  hin,visitedAt);
        } else {
            // Retrieve patient
            patient = Patient.findById(id);
            // Edit
                patient.firstName=firstName;
                patient.lastName=lastName;
                patient.address=address;
                patient.phoneNumber=phoneNumber;
                patient.birthAt=birthAt;
                patient.gender=gender;
                patient.healthStatus=healthStatus;
                patient.hin=hin;
                patient.visitedAt=visitedAt;
        }
        // Validate
        validation.valid(patient);
        if(validation.hasErrors()) {
            render("Patient/Admin/form.html", patient);
        }
        // Save
        patient.save();
        AppPatient.patient_table();
    }

    //Show the Patient
    public static void show(Long id){
        Patient patient=Patient.findById(id);
        renderTemplate("Patient/Admin/show.html",patient);
    }
    
    //Delete Patient
    public static void delete(Long id){
        Patient patient=Patient.findById(id);
        patient.delete();
        AppPatient.patient_table();
    }

/*
    Adding comment to Patient 

*/
    public static void postTreatment(Long patientId,
				   @Required String author,
				   @Required String content) {
	Patient patient = Patient.findById(patientId);
	if (validation.hasErrors()) {
            params.flash();
	    render("Patient/Admin/show.html", patient);
	}
	patient.addTreatment(author, content);
	show(patientId);
    }

    //Document
    public static void uploadDocument(Document document) {
        document.save();
        patientDocument();
        show(document.id);
    }
    
    public static void deleteDocument(Long id) {
        Document document=Document.findById(id);
        document.delete();
        patientDocument();
    }
    
    /*
    public static void getDocument(long id) {
        Document document = Document.findById(id);       
        renderBinary(document.document.get());
    }
    */
    
    public static void getdocument(Long id) {
        // Fetch user from DB
        Document document=Document.findById(id);
        // Check image availability
        if (document.document != null && document.document.exists()) {
                // Send image
                response.contentType = document.document.type();
                renderBinary(document.document.get(), document.document.length());
        } else {
                // Send 404
                notFound();
        }
    }
    
    //Search patient
    public static void searchDocument(String search) {
    	Query query;
    	String highlight;
    	
    	if (search.contains(":")) {
    		query = Search.search(search, Document.class);
    		highlight = search.split(":", 2)[1];
    	} else {
    		query = Search.search("firstName:"+search, Document.class);
    		highlight = search;
    	}
    	List results = query.fetch();
    	renderTemplate("Documents/searchDocument.html",results, search, highlight);
    }

    //Bill generation method.
    /*
    public static void getDocument(long id) {
        Document document = Document.findById(id);       
        renderPDF("Patient/patient_documents.html",document.document.get());
    }
    */
    
/*   Patient Reminders   */
    
    //Reminder Form
    //Form
    public static void taskForm(Long id) {
        if(id != null) {
            PatientTask patientTask = PatientTask.findById(id);
            render("Patient/Admin/task/form.html",patientTask);
        }
        renderTemplate("Patient/Admin/task/form.html");
    }
    
    //Add Reminders
    
    public static void addTask(
                    Long id,
                    String doctor,
                    String task,
                    String note,
                    Date createDate,
                    Date dueDate
                    ) {
        PatientTask patientTask;
        if(id == null) {
            // Create patient
            User author = User.find("byEmail", Security.connected()).first();
            patientTask = new PatientTask(author,doctor,task,note,createDate,dueDate);
        } else {
            // Retrieve patient
            patientTask = PatientTask.findById(id);
            // Edit
                    patientTask.doctor=doctor;
                    patientTask.task=task;
                    patientTask.note = note;
                    patientTask.createDate = new Date();
                    patientTask.dueDate=dueDate;
        }
        // Validate
        validation.valid(patientTask);
        if(validation.hasErrors()) {
            render("Patient/Admin/task/form.html",patientTask);
        }
        // Save
        patientTask.save();
        AppPatient.patientReminder();
    }
    
}