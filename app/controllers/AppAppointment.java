package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import play.data.validation.*;
import models.*;


@With(Secure.class)
public class AppAppointment extends Controller {

    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            User user = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", user.fullname);
        }
    }
    
    public static void index() {
        render("Appointments/index.html");
    }

    public static void calender() {
        render("Appointments/calender.html");
    }

}