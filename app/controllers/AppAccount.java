package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import play.data.validation.*;
import models.*;

//PDF modules
import static play.modules.pdf.PDF.*;

@With(Secure.class)
public class AppAccount extends Controller {

    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            User user = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", user.fullname);
        }
    }
    
    public static void index() {
        render("Account/index.html");
    }
    
    public static void dentalCode_table(){
        System.out.println("DentoX2:Dental Code");
        List<DentalCode> dentalCode=DentalCode.find(
            "order by id asc"
        ).from(0).fetch(20);
        render("Account/dental_code.html",dentalCode);
    }
    
    public static void dentalCodeForm(Long id) {
        if(id != null) {
            DentalCode dentalCode = DentalCode.findById(id);
            render("DentalCodes/form.html",dentalCode);
        }
        renderTemplate("DentalCodes/form.html");
    }
    
    //Save
    public static void saveDentalCode(Long id,
                    String code,
                    Long unit_cost,
                    String desc
                    ) {
        DentalCode dentalCode;
        if(id == null) {
            // Create patient
            User author = User.find("byEmail", Security.connected()).first();
            dentalCode = new DentalCode(author,code,unit_cost,desc);
        } else {
            // Retrieve patient
            dentalCode = DentalCode.findById(id);
            // Edit
                dentalCode.code=code;
                dentalCode.unit_cost=unit_cost;
                dentalCode.desc=desc;
        }
        // Validate
        validation.valid(dentalCode);
        if(validation.hasErrors()) {
            render("DentalCodes/form.html", dentalCode);
        }
        // Save
        dentalCode.save();
        AppAccount.dentalCode_table();
    }
    
    //Bill generation method.
    //show Dental codes list
	
	public static void getDocument() {
	        List<DentalCode> dentalCode=DentalCode.find( "order by id asc").from(0).fetch(20);
	        renderPDF("Account/dental_code.html",dentalCode);
	}

    
    /*
    //Save or Create DentalCodes
    public static void saveDentalCode(Long id,
                                    String code,
                                    Long unit_cost,
                                    String desc
                            ) {
        DentalCode dentalCode;
        if(id == null) {
            // Create patient
            User author = User.find("byEmail", Security.connected()).first();
            dentalCode = new DentalCode(code,unit_cost,desc);                 
        } else {
            // Retrieve patient
                dentalCode = DentalCode.findById(id);
            // Edit
                dentalCode.code=code;
                dentalCode.unit_cost=unit_cost;
                dentalCode.desc=desc;
            }
        // Validate
        validation.valid(dentalCode);
        if(validation.hasErrors()) {
            render("DentalCodes/form.html", dentalCode);
        }
        // Save
        dentalCode.save();
        AppAccount.dentalCode_table();
    }
    */
}