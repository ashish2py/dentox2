package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import play.data.validation.*;
import models.*;

@With(Secure.class)
public class AppOffice extends Controller {

    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            User user = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", user.fullname);
        }
    }
    
    //index
    public static void index(){
        System.out.println("DentoX2 : Office !!");
        render("Office/office.html");
    }
//#~~~~~~~~~~~~~~~
//Clinics
//#~~~~~~~~~~~~~
    public static void clinic_table(){
        System.out.println("DentoX2:clinic history");
        List<Clinic> clinics=Clinic.find(
            "order by id asc"
        ).from(0).fetch(20);
        render("Office/clinic_table.html",clinics);
    }
    
    //Clinic Form
    
    public static void clinicForm(Long id) {
    if(id != null) {
        Clinic clinic = Clinic.findById(id);
        render("Clinics/form.html",clinic);
    }
    renderTemplate("Clinics/form.html");
    }

    //Add Clinic    
    public static void saveClinic(Long id,String clinicID,
                        String name,String branch,Long phoneNumber,
                        Date openAt,Date closeAt
                        ) {
    Clinic clinic;
    if(id == null) {
        // Create patient
        User author = User.find("byEmail", Security.connected()).first();
        clinic = new Clinic(author,clinicID,name,branch,phoneNumber,openAt,closeAt);
    }
    else {
        // Retrieve patient
        clinic = Clinic.findById(id);
        // Edit
        clinic.clinicID=clinicID;
        clinic.name = name;
        clinic.branch = branch;
        clinic.phoneNumber=phoneNumber;
        clinic.openAt=openAt;
        clinic.closeAt=closeAt;
    }
    // Validate
    validation.valid(clinic);
    if(validation.hasErrors()) {
        render("Clinics/form.html", clinic);
    }
        // Save
    clinic.save();
    AppOffice.clinic_table();

    }
    
    //Delete Clinic History
    public static void deleteClinic(Long id){
        Clinic clinic=Clinic.findById(id);
        clinic.delete();
        AppOffice.clinic_table();
    }
    //Show Details of Clinic
    public static void showClinic(Long id){
        Clinic clinic=Clinic.findById(id);
        renderTemplate("Clinics/show.html",clinic);
    }
    


//~~~~~~~~~~~~~~~~~    
//Doctors
//~~~~~~~~~~~~~~~~~    
    public static void doctor_table(){
        System.out.println("DentoX2:Doctor history");
        List<Doctor> doctors=Doctor.find(
            "order by id asc"
        ).from(0).fetch(20);
        render("Office/doctor_table.html",doctors);
    }
    
    public static void doctorForm(Long id) {
        if(id != null) {
            Doctor doctor = Doctor.findById(id);
            render("Doctors/form.html",doctor);
        }
        renderTemplate("Doctors/form.html");
    }
    
    //Save or Create Doctors
    public static void saveDoctor(
                    Long id,String PIN,
                    String firstName,String lastName,
                    Long phoneNumber,String address,
                    String gender,Date birthAt,
                    String licenceNo,String specialities,
                    Long emergencyNo,Date joinedAt
                            ) {
        Doctor doctor;
        if(id == null) {
            // Create patient
            User author = User.find("byEmail", Security.connected()).first();
            doctor = new Doctor(
                                author,PIN, firstName,
                                lastName,phoneNumber,
                                address,gender,birthAt,
                                licenceNo,specialities,
                                emergencyNo,joinedAt);
        } else {
            // Retrieve patient
            doctor = Doctor.findById(id);
            // Edit
            doctor.PIN=PIN;
            doctor.firstName=firstName;
            doctor.lastName=lastName;
            doctor.phoneNumber=phoneNumber;
            doctor.address=address;
            doctor.gender=gender;
            doctor.birthAt=birthAt;
            doctor.licenceNo=licenceNo;
            doctor.specialities=specialities;
            doctor.emergencyNo=emergencyNo;
            doctor.joinedAt=joinedAt;
        }
        // Validate
        validation.valid(doctor);
        if(validation.hasErrors()) {
            render("Doctors/form.html", doctor);
        }
        // Save
        doctor.save();
        AppOffice.doctor_table();
    }

    //Delete Docotrs
    public static void deleteDoctor(Long id){
        Doctor doctor=Doctor.findById(id);
        doctor.delete();
        AppOffice.doctor_table();
    }
    //Show Details of Doctors
    public static void showDoctor(Long id){
        Doctor doctor=Doctor.findById(id);
        renderTemplate("Doctors/show.html",doctor);
    }

/*
    Dentist Shedule
*/
    public static void dentistShedule(){
        System.out.println("DentoX2:Dentist Shedule");
        List<DentistShedule> dentShedule=DentistShedule.find(
                "order by id asc"
        ).from(0).fetch(20);
        renderTemplate("Office/dentist_shedule.html",dentShedule);
    }

/*
Form and Save the Dentist shedule
*/

//Clinic Form
    
    public static void dentistSheduleForm(Long id) {
    if(id != null) {
        DentistShedule dentShedule = DentistShedule.findById(id);
        render("DentistShedules/form.html",dentShedule);
    }
    renderTemplate("DentistShedules/form.html");
    }

    //Add Clinic    
    public static void saveDentistShedule(Long id,String PIN,String firstName,
                        String lastName,String gender,
                        Date startAt,Date endAt
                        ) {
    DentistShedule dentShedule;
    if(id == null) {
        // Create patient
        User author = User.find("byEmail", Security.connected()).first();
        dentShedule = new DentistShedule(author,PIN,firstName,lastName,gender,
                                         startAt,endAt);
    }
    else {
        // Retrieve patient
        dentShedule = DentistShedule.findById(id);
        // Edit
        dentShedule.PIN=PIN;
        dentShedule.firstName=firstName;
        dentShedule.lastName=lastName;
        dentShedule.gender=gender;
        dentShedule.startAt=startAt;
        dentShedule.endAt=endAt;
    }
    // Validate
    validation.valid(dentShedule);
    if(validation.hasErrors()) {
        render("DentistShedules/form.html", dentShedule);
    }
        // Save
    dentShedule.save();
    AppOffice.dentistShedule();

    }
    
    //Delete Clinic History
    public static void deleteDentistShedule(Long id){
        DentistShedule dentShedule=DentistShedule.findById(id);
        dentShedule.delete();
        AppOffice.dentistShedule();
    }
    //Show Details of Clinic
    public static void showDentistShedule(Long id){
        DentistShedule dentShedule=DentistShedule.findById(id);
        renderTemplate("DentistShedules/show.html",dentShedule);
    }
    



/*
//#~~~~~~~~~~~~~~~~~~~~~~
//Clinic
//#~~~~~~~~~~~~~~~~~~~~~~~
    //Clinic History
    public static void clinic_history(){
        System.out.println("DentoX2:clinic history");
        List<Clinic> clinics=Clinic.find(
            "order by id asc"
        ).from(0).fetch(20);
        render("Office/clinic_history.html",clinics);
    }
    
    //Clinic Form
    public static void clinicform(Long id) {
        if(id != null) {
            Clinic clinic = Clinic.findById(id);
            render("Clinics/form.html",clinic);
        }
        renderTemplate("Clinics/form.html");
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`
    //Add Clinic    
    public static void addClinic(Long id,String clinicID,
                        String name,String branch,Long phoneNumber,
                        Date openAt,Date closeAt
                        ) {
    Clinic clinic;
    if(id == null) {
        // Create patient
        User author = User.find("byEmail", Security.connected()).first();
        clinic = new Clinic(author,clinicID,name,branch,phoneNumber,openAt,closeAt);
    }
    else {
        // Retrieve patient
        clinic = Clinic.findById(id);
        // Edit
        clinic.clinicID=clinicID;
        clinic.name = name;
        clinic.branch = branch;
        clinic.phoneNumber=phoneNumber;
        clinic.openAt=openAt;
        clinic.closeAt=closeAt;
    }
    // Validate
    validation.valid(clinic);
    if(validation.hasErrors()) {
        render("Clinics/form.html", clinic);
    }
        // Save
    clinic.save();
    AppOffice.clinic_history();

    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`       
    public static void showDoctor(Long id){
        Doctor doctor=Doctor.findById(id);
        renderTemplate("Doctors/show.html",doctor);
    }
    
    //Delete
    public static void deleteclinic(Long id){
        Clinic clinic = Clinic.findById(id);
        clinic.delete();
        AppOffice.clinic_history();
    }
*/  
}