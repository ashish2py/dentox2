
package models ;

import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;

@Entity
public class Clinic extends Model{

    @Required(message = "Enter Clinic ID !")
    public String clinicID;

    @Required(message = "Enter Clinic Name!")
    @MinSize(3)
    @MaxSize(30)
    public String name;
    
    @Lob
    @MaxSize(50)
    @Required
    public String branch;
        
    @Required(message = "Enter valid number !")
    @MaxSize(10)
    public Long phoneNumber;
    
    
    @Required(message = "Enter valid Date !")
    public Date openAt;

    @Required(message = "Enter valid Date !")
    public Date closeAt;
    
    
    @Required
    @ManyToOne
    public User author;
    
    public Clinic (User author,
                    String clinicID,String name,String branch,
                    Long phoneNumber,Date openAt,Date closeAt
                    ){
        this.author=author;
        this.clinicID=clinicID;
        this.name=name;
        this.branch=branch;
        this.phoneNumber=phoneNumber;
        this.openAt=openAt;
        this.closeAt=closeAt;
    }

    public String toString(){
        return name;
    }

} 