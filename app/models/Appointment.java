
package models ;
    
import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;
import play.modules.search.*;
import javax.persistence.Entity;
import org.joda.time.DateTime;
import play.db.jpa.Model;

@Entity
@Indexed
public class Appointment extends Model{

    @Field
    @Required(message = "Enter First Name !")
    @MinSize(3)
    @MaxSize(13)
    public String firstName;
    
    @Field
    @Required(message = "Enter Last Name !")
    @MinSize(3)
    @MaxSize(13)
    public String lastName;


    @Required(message = "Please,check it !")
    @MaxSize(6)
    public String gender;
    
    
    @Required(message = "Enter valid number !")
    @MaxSize(13)
    public Long phoneNumber;
    
    @Lob
    @MaxSize(500)
    @Required
    public String address;
    
    @Required(message = "Enter valid Date !")
    public Date startDateAt;
    
    @Required
    public Long startTimeAt;
    
    @Required(message = "Enter valid Date !")
    public Date endDateAt;
    
    @Required
    public Long endTimeAt;

    @Required
    @ManyToOne
    public User author;
    
    @OneToMany(mappedBy="patient", cascade=CascadeType.ALL)
    public List<Comment> comments;

    public Appointment (User author,
                    String firstName,String lastName,String gender,
                    Long phoneNumber,String address,
                    Date startAt,Long startTimeAt,
                    Date endAt,Long endTimeAt
                    ){
/*
        this.treatments = new ArrayList<Treatment>();
*/
        this.comments = new ArrayList<Comment>();
        this.author=author;
        this.firstName=firstName;
        this.lastName=lastName;
        this.gender=gender;
        this.phoneNumber=phoneNumber;
        this.address=address;
        this.startDateAt=startDateAt;
        this.startTimeAt=startTimeAt;
        this.endDateAt=endDateAt;
        this.endTimeAt=endTimeAt;
    }
    
    //~~~~~~~~~~~~~~~~~~~~~    

    public String toString(){
        return firstName;
    }

} 