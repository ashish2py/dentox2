
package models ;
    
import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;
import play.modules.search.*;
 
@Entity
public class Treatment extends Model {
 
    public String author;
    public Date postedAt;
     
    @Lob
    public String content;
    
    @ManyToOne
    public Patient patient;
    
    public Treatment(Patient patient, String author, String content) {
        this.patient = patient;
        this.author = author;
        this.content = content;
        this.postedAt = new Date();
    }
 
}
