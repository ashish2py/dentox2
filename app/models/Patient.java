
package models ;
    
import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;
import play.modules.search.*;

@Entity
@Indexed
public class Patient extends Model{

    @Field
    @Required(message = "Enter First Name !")
    @MinSize(3)
    @MaxSize(13)
    public String firstName;
    
    @Field
    @Required(message = "Enter Last Name !")
    @MinSize(3)
    @MaxSize(13)
    public String lastName;

    @Lob
    @MaxSize(500)
    @Required
    public String address;
    
    @Required(message = "Enter valid number !")
    @MaxSize(13)
    public Long phoneNumber;
    
    
    @Required(message="Enter valid Date !")
    public Date birthAt;
    
    @Required(message = "Please,check it !")
    @MaxSize(6)
    public String gender;
    
    @MaxSize(500)
    @Lob
    public String healthStatus;
    
    @Required(message="Enter proper HIN !")
    @MaxSize(10)
    public String hin;
    
    @Required(message = "Enter valid Date !")
    public Date visitedAt;
    
    @Required
    @ManyToOne
    public User author;
    
    @OneToMany(mappedBy="patient", cascade=CascadeType.ALL)
    public List<Treatment> treatments;

    public Patient (User author,
                    String firstName,String lastName,String address,
                    Long phoneNumber,Date birthAt,String gender,
                    String healthStatus,String hin,
                    Date visitedAt
                    ){
/*
        this.treatments = new ArrayList<Treatment>();
*/
        this.treatments = new ArrayList<Treatment>();
        this.author=author;
        this.firstName=firstName;
        this.lastName=lastName;
        this.address=address;
        this.phoneNumber=phoneNumber;
        this.birthAt=birthAt;
        this.gender=gender;
        this.healthStatus=healthStatus;
        this.hin=hin;
        this.visitedAt=visitedAt;
    }
    
    /* ~~~~~~~~~~~~~~~~~~~~
    Add Treatments
    */
    public Patient addTreatment(String author, String content) {
        Treatment newTreatment = new Treatment(this, author, content).save();
        this.treatments.add(newTreatment);
        this.save();
        return this;
    }
    
    /*
    public Patient previous() {
        return Patient.find("visitedAt < ? order by visitedAt desc", visitedAt).first();
    }
    
    public Patient next() {
        return Patient.find("visitedAt > ? order by visitedAt asc", visitedAt).first();
    }
    */

    //~~~~~~~~~~~~~~~~~~~~~    

    public String toString(){
        return firstName;
    }
    
    

} 