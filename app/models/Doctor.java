
package models ;

import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;
import play.modules.search.*;

@Entity
@Indexed
public class Doctor extends Model{

    @Field
    @Required(message="Enter Personal Information No.")
    public String PIN;
    
    @Field
    @Required(message = "Enter First Name!")
    @MinSize(3)
    @MaxSize(13)
    public String firstName;
    
    @Required(message = "Enter Last Name!")
    @MinSize(3)
    @MaxSize(13)
    public String lastName;

    @Required(message = "Enter valid number !")
    @MaxSize(13)
    public Long phoneNumber;
    
    @Lob
    @MaxSize(500)
    @Required
    public String address;
  
    @Required(message = "Please,check it !")
    @MaxSize(6)
    public String gender;
    
    @Required(message = "Enter birth date !")
    public Date birthAt;

    @Required
    public String licenceNo;
    
    @Required
    public String specialities;
    
    @Required(message = "Enter valid number !")
    @MaxSize(13)
    public Long emergencyNo;

    @Required(message = "Enter date of joining !")
    public Date joinedAt;
    
    @Required
    @ManyToOne
    public User author;
    
    public Doctor (User author,String PIN,String firstName,
                   String lastName,Long phoneNumber,
                   String address,String gender,Date birthAt,
                   String licenceNo,String specialities,Long emergencyNo,Date joinedAt
                    ){
        this.author=author;
        this.PIN=PIN;
        this.firstName=firstName;
        this.lastName=lastName;
        this.phoneNumber=phoneNumber;
        this.address=address;
        this.gender=gender;
        this.birthAt=birthAt;
        this.licenceNo=licenceNo;
        this.specialities=specialities;
        this.emergencyNo=emergencyNo;
        this.joinedAt=joinedAt;
    }

    public String toString(){
        return firstName;
    }

}