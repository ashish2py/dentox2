
package models ;

import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;
import play.modules.search.*;

@Entity
@Indexed
public class DentistShedule extends Model{

    @Field
    @Required(message="Enter Personal Information No.")
    public String PIN;
    
    @Field
    @Required(message = "Enter First Name!")
    @MinSize(3)
    @MaxSize(13)
    public String firstName;

    @Required(message = "Enter Last Name!")
    @MinSize(3)
    @MaxSize(13)
    public String lastName;
  
    @Required(message = "Please,check it !")
    @MaxSize(6)
    public String gender;
    
    @Required(message = "In time !")
    public Date startAt;

    public Date endAt;
    
    @Required
    @ManyToOne
    public User author;
    
    public DentistShedule (User author,String PIN,String firstName,
                   String lastName,String gender,Date startAt,
                   Date endAt
                    ){
        this.author=author;
        this.PIN=PIN;
        this.firstName=firstName;
        this.lastName=lastName;
        this.gender=gender;
        this.startAt=startAt;
        this.endAt=endAt;
    }

    public String toString(){
        return firstName;
    }

}