package models;
 
import java.util.*;
import javax.persistence.*;
import play.data.validation.*;
import play.data.validation.Error.*; 
import play.db.jpa.*;
 
@Entity
public class PatientTask extends Model {

    @Required(message = "Enter Doctor's Name !")
    @MinSize(3)
    @MaxSize(13) 
    public String doctor;
    
    @Required(message = "Enter Task to do !")
    public String task;
    
    @Required(message = "Enter Notes !")
    public String note;
    
    public Date createDate;
    
    @Required(message = "Enter Due Date !")
    public Date dueDate;

    
    @ManyToOne
    @Required
    public User author;
    
    public PatientTask(User author, String doctor,String task,
                       String note,Date createdAt,Date dueDate
                       ) {
        this.author = author;
        this.doctor=doctor;
        this.task=task;
        this.note = note;
        this.createDate = new Date();
        this.dueDate=dueDate;
    }
    
 
}
