
package models ;

import play.*;
import play.mvc.*;

import java.util.*;
import javax.persistence.*;
import javax.persistence.Entity;
import play.db.jpa.*;
import play.db.jpa.Blob;
import play.db.jpa.Model;
import play.data.validation.*;
import play.data.validation.Error.*;
import play.modules.search.*;

@Entity
@Indexed
public class Document extends Model {
   
   @Field
   @Required
   public String firstName;
    
   @Required
   @MaxSize(30)
   public String comment;
        
   public Blob document;
    
   @Required
   @ManyToOne
   public User author;
    
   public Document(User author,String firstName,String comment,Blob document){
        this.author=author;
        this.firstName=firstName;
        this.comment=comment;
   }
    
   public String toString(){
      return firstName;
   }

}