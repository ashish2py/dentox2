
package models ;

import java.util.*;
import javax.persistence.*;

import play.db.jpa.*;
import play.data.validation.*;

@Entity
public class DentalCode extends Model{
  
    @Required(message = "Enter valid code !")
    @MinSize(3)
    @MaxSize(13)
    public String code;
    
    @Required(message = "Enter cost in INR !")
    public Long unit_cost;
    
    @Required(message = "Enter Description !")
    @MinSize(3)
    public String desc;

    @Required
    @ManyToOne
    public User author;
    
    public DentalCode (User author,
                    String code,
                    Long unit_cost,
                    String desc
                    ){
        this.author=author;
        this.code=code;
        this.unit_cost=unit_cost;
        this.desc=desc;
    }
    
    public String toString(){
        return desc;
    }

}