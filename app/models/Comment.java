package models;
 
import java.util.*;
import javax.persistence.*;
 
import play.db.jpa.*;
 
@Entity
public class Comment extends Model {
 
    public String author;
    public Date postedAt;
     
    @Lob
    public String content;
    
    @ManyToOne
    public Patient patient;
    
    public Comment(Patient patient, String author, String content) {
        this.patient = patient;
        this.author = author;
        this.content = content;
        this.postedAt = new Date();
    }
 
}
